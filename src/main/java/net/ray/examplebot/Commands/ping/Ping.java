package net.ray.examplebot.Commands.ping;

import de.btobastian.sdcf4j.Command;
import de.btobastian.sdcf4j.CommandExecutor;
import net.dv8tion.jda.core.entities.Channel;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.User;

// Class implementing command executor as needed from the JDA library
public class Ping implements CommandExecutor {

	/*
	 * you can have as many alias' as you need and provide a description for a built
	 * in help menu you can control if it can be use in private messages and if its
	 * async or not in the on command you can add arguments such as 'User usr' like
	 * be low and use that in the code and it will return things the user who sent
	 * the command's avatar, longid, string name etc.
	 * 
	 * you can see a return which is a string but if its a public void you dont have
	 * to return a string but to send a message look below (1) REMEMBER you have to
	 * queue the message or it wont send through
	 */
	@Command(aliases = { "ping", "p" }, description = "Pong!", async = true, privateMessages = true)
	public String onCommand(String[] args, Message msg, User usr, Channel chnl, Guild gld) {

		// (1)
		msg.getChannel().sendMessage("Pong!").queue();

		return "pong";
	}
}
