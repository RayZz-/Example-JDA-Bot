package net.ray.examplebot.Commands;

import org.apache.log4j.Logger;

import de.btobastian.sdcf4j.CommandHandler;
import de.btobastian.sdcf4j.handler.JDA3Handler;
import net.ray.examplebot.Main;
import net.ray.examplebot.Commands.ping.Ping;

public class RegisterCommands implements Runnable {

	// Logger for this class
	Logger logger = Logger.getLogger(RegisterCommands.class);

	// everything to be run in a separate thread is done in the run
	// method/function
	@Override
	public void run() {

		CommandHandler cmdHandler = new JDA3Handler(Main.client);

		// Set default prefix
		cmdHandler.setDefaultPrefix("#");

		// Register ping command
		cmdHandler.registerCommand(new Ping());
	}

}
