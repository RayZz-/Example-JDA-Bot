// Package name at top always prolly changed in later versions of java
package net.ray.examplebot;
// imports below

import javax.security.auth.login.LoginException;

import org.apache.log4j.Logger;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.ray.examplebot.Commands.RegisterCommands;

// class here
public class Main {

	// Creating Threads
	static Thread regcmds = new Thread(new RegisterCommands());

	// usually i keep variables here
	static String s;
	String st = "string";

	// logger for this class
	static Logger logger = Logger.getLogger(Main.class);

	// To allow access of the instance to other classes
	public static JDA client;

	// main method/function here everything starts here
	public static void main(String[] args) {

		// s would need to be static to use it in a static
		// method/function you can get around this in a way
		s = "string";

		try {
			// Connect to discord using JDA through a bot account type
			// using token and then finally build it then log saying its
			// connected to discord with no errors
			JDA jda = new JDABuilder(AccountType.BOT).setToken("token").buildBlocking();
			client = jda;
			logger.debug(JDA.Status.CONNECTED.toString());

		} catch (LoginException | InterruptedException e) {
			// log exception through Log4j
			logger.error(e);
		}

		// Start register commands thread
		// check the class for more info
		regcmds.start();

	}

}
